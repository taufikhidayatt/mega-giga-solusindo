import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
const axios = require('axios');

Vue.config.productionTip = false

axios.defaults.baseURL = 'http://localhost:8080/api';
axios.defaults.headers.post['Content-Type'] = 'application/json';

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
